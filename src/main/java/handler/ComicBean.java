package handler;

import java.io.Serializable;

/**
 * Created by kATHRYN on 4/26/2016.
 */
public class ComicBean implements Serializable {

    private Long id;
    private String save;

    public void setId(Long id) {
        this.id = id;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public Long getId() { return id; }

    public String getSave() { return save; }
}
