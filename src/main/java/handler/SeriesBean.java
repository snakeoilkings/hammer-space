package handler;

import entity.Episode;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kATHRYN on 5/1/2016.
 */
public class SeriesBean implements Serializable {

    private String link;
    private String author;
    private String name;
    private String pic;
    private String description;
    private Long id;
    private ArrayList<Episode> episodes;

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setDescription(String description){ this.description = description; }

    public String getLink() { return link; }

    public String getPic() { return pic; }

    public String getName() { return name; }

    public void setEpisodes(ArrayList<Episode> eps) { this.episodes = eps; }

    public ArrayList<Episode> getEpisodes() { return episodes; }

    public void setId(Long id) {this.id = id;}

    public Long getId() { return id; }

    public void setAuthor(String author) { this.author = author; }

    public String getAuthor() { return author; }

    public String getDescription() { return description; }

}
