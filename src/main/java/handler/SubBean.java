package handler;

import java.io.Serializable;

/**
 * Created by kATHRYN on 5/2/2016.
 */
public class SubBean implements Serializable {

    private Long id;
    private String title;

    public Long getId() { return id; }

    public String getTitle() { return title; }

    public void setId(Long id) { this.id = id; }

    public void setTitle(String title) { this.title = title; }

}
