package com.cse308;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import entity.Account;
import entity.Episode;
import entity.Series;
import handler.SeriesBean;
import handler.SubBean;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kathryn on 4/5/2016.
 */
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        String loginUrl = userService.createLoginURL("/");
        String logoutUrl = userService.createLogoutURL("/logout");

        /* create blobstore service for image upload */
        BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
        String uploadUrl = blobstoreService.createUploadUrl("/create_account");
        request.setAttribute("uploadUrl", uploadUrl);

        /* create session objects */
        HttpSession session = request.getSession();
        session.setAttribute("user", null);
        session.setAttribute("logoutUrl", logoutUrl);
        session.setAttribute("loginUrl", loginUrl);

        request.setAttribute("account", null);

        /* populate images for scrolling */
        int j = 0;
        List<Series> seriesList = ObjectifyService.ofy()
                .load()
                .type(Series.class)
                .filter("seriesPic !=", null)
                .limit(9)
                .list();
        if (seriesList.size()>=3) {
            for (int i=0; i < 9; i++) {
                if (seriesList.size() <= j)
                    j = 0;
                request.setAttribute("pic" + (i + 1), "/serve?blob-key=" + seriesList.get(j).getSeriesPic());
                if (seriesList.get(j) != null && seriesList.get(j).getSeriesID() != null) {
                    request.setAttribute("link" + (j + 1), "/series/" + seriesList.get(j).getSeriesID());
                    request.setAttribute("title" + (j + 1), seriesList.get(j).getTitle());
                }
                j++;
            }
        }

        /* populate category images */
        List<Series> seriesList2 = ObjectifyService.ofy()
                .load()
                .type(Series.class)
                .filter("seriesPic !=", null)
                .limit(3)
                .list();
        if (seriesList2.size()>=3) {
            for (int i=0; i < 3; i++) {
                if (seriesList2.get(i) != null && seriesList2.get(i).getSeriesPic() != null && seriesList2.get(i).getCategory()!=null) {
                    request.setAttribute("cat_pic" + (i + 1), "/serve?blob-key=" + seriesList2.get(i).getSeriesPic());
                    request.setAttribute("cat" + (i + 1), seriesList2.get(i).getCategory());
                    if (seriesList2.get(i) != null && seriesList2.get(i).getSeriesID() != null)
                        request.setAttribute("cat_link" + (i + 1), "/series/" + seriesList2.get(i).getSeriesID().toString());
                }
            }
        }

        /* populate top episodes  TODO: make a better mechanism for finding top episodes */
        List<Episode> episodeList = ObjectifyService.ofy()
                .load()
                .type(Episode.class)
                .order("-likes")
                .limit(4)
                .list();
        if (episodeList.size()>=4) {
            for (int i=0; i < 4; i++) {
                if (episodeList.get(i) != null) {
                    Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", episodeList.get(i).getSeriesID()).first().now();
                    request.setAttribute("pop_link" + (i + 1), "/view/" + episodeList.get(i).getComicID());
                    if (s != null) {
                        request.setAttribute("pop_pic" + (i + 1), "/serve?blob-key=" + s.getSeriesPic());
                        request.setAttribute("pop_name" + (i + 1), s.getTitle() + ": " + episodeList.get(i).getTitle());
                    }
                }
            }
        }


        if (user!=null) {
            String email = user.getNickname();
            entity.User hasAccount = null;
            hasAccount = ObjectifyService.ofy().load().type(entity.User.class).filter("email", email).first().now();

            /* no account available, prepares account setup */
            if (hasAccount==null) {
                session.setAttribute("user", email);
            }
            /* loads account info otherwise */
            else {
                String username = hasAccount.getUsername();
                ArrayList<SubBean> subscrips = new ArrayList();
                Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", username).first().now();

                session.setAttribute("account", username);
                session.setAttribute("user", email);
                for(Long subs:a.subscriptionList){
                    SubBean sb = new SubBean();
                    sb.setId(subs);
                    sb.setTitle(ObjectifyService.ofy().load().type(Series.class).filter("seriesID", subs).first().now().getTitle());
                    subscrips.add(sb);
                }
                session.setAttribute("subs", subscrips);
                session.setAttribute("recent", a.recentlyComicList);

                if (!a.seriesList.isEmpty()) {
                    ArrayList<SeriesBean> series = new ArrayList();
                    for (Long id : a.getSeriesList()) {
                        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", id).first().now();
                        SeriesBean t = new SeriesBean();

                        if (s.getSeriesPic() != null) {
                            t.setPic("/serve?blob-key=" + s.getSeriesPic());
                            t.setName(s.getTitle());
                            t.setAuthor(s.getAuthor());
                            t.setId(s.getSeriesID());
                        }
                        else {
                            t.setPic(null);
                            t.setName(null);
                            t.setAuthor(null);
                            t.setId(null);
                        }

                        t.setLink("/series/" + id.toString());
                        series.add(t);
                    }

                    session.setAttribute("series", series);
                }
                session.setAttribute("comments", a.commentList);
            }
        }

        request.setAttribute("user", user);
        request.setAttribute("loginUrl", loginUrl);
        request.setAttribute("logoutUrl", logoutUrl);

        response.setContentType("text/html");

        RequestDispatcher jsp = request.getRequestDispatcher("/index.jsp");
        jsp.forward(request, response);
    }
}
