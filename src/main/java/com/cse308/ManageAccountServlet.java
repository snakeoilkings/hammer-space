package com.cse308;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Series;
import entity.User;
import handler.SeriesBean;
import handler.SubBean;

/**
 * Created by kATHRYN on 4/20/16.
 */
public class ManageAccountServlet extends HttpServlet {
    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Map<String, List<BlobKey>> blobs = this.blobstoreService.getUploads(request);

        List<BlobKey> blobKeys = blobs.get("aFile");

        User usr = ObjectifyService.ofy().load().type(User.class).filter("username", request.getSession().getAttribute("account")).first().now();
        Account act = ObjectifyService.ofy().load().type(Account.class).filter("username", request.getSession().getAttribute("account")).first().now();

        if (blobKeys != null)
            usr.setProfilePic(blobKeys.get(0).getKeyString());

        request.getSession().setAttribute("blobkey", usr.getProfilePic());

        String selected[] = request.getParameterValues("unsub");
        if (selected != null && selected.length != 0)
            for (int i = 0; i < selected.length; i++) {
                act.removeSubscription(Long.parseLong(selected[i]));
            }

        ObjectifyService.ofy().save().entity(act).now();
        ObjectifyService.ofy().save().entity(usr).now();

        ArrayList<SubBean> subscrips = new ArrayList();
        HttpSession session = request.getSession();
        for(Long subs:act.subscriptionList){
            SubBean sb = new SubBean();
            sb.setId(subs);
            sb.setTitle(ObjectifyService.ofy().load().type(Series.class).filter("seriesID", subs).first().now().getTitle());
            subscrips.add(sb);
        }
        session.setAttribute("subs", subscrips);

        String redirectURL = "/manageAccount.jsp";
        response.sendRedirect(redirectURL);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
