package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;
import entity.Series;
import handler.SubBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kATHRYN on 4/27/2016.
 */
public class ViewEpisodeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURL().toString();
        String[] urlParts = url.split("/");
        Long comicID = Long.parseLong(urlParts[urlParts.length-1]);

        Episode ep = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", comicID).first().now();
        Long seriesID = ep.getSeriesID();
        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", seriesID).first().now();

        ArrayList<Long> episodes = s.getEpisodeList(); Long prevID=comicID, nextID=comicID;
        for(int i=0; i < episodes.size(); i++) {
            if (episodes.get(i).longValue() == comicID.longValue()) {
                if (i != 0)
                    prevID = episodes.get(i - 1);
                if (i != episodes.size() - 1)
                    nextID = episodes.get(i + 1);
            }
        }

        // find out if they are already subbed to this series
        ArrayList<SubBean> sublist = (ArrayList<SubBean>)request.getSession().getAttribute("subs");
        if (sublist != null) {
            for (SubBean sub: sublist) {
                if ((long)sub.getId()==(long)seriesID)
                    request.setAttribute("subbed", "true");
            }
        }


        if (ep == null || s == null) {
            //set up 404
        }
        else {
            request.setAttribute("likes", ep.getLikes());
            request.setAttribute("seriesID", seriesID);
            request.setAttribute("author", s.getAuthor());
            request.setAttribute("series", s.getTitle());
            request.setAttribute("title", ep.getTitle());
            request.setAttribute("comic", ep.getUrlString());
            request.setAttribute("comicID", comicID);
            request.setAttribute("next", "/view/"+nextID.toString());
            request.setAttribute("prev", "/view/"+prevID.toString());
            request.setAttribute("comments", ep.getCommentList());
        }

        RequestDispatcher jsp = request.getRequestDispatcher("/viewcomic.jsp");
        jsp.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
