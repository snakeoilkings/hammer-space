package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;
import entity.Series;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 5/5/2016.
 */
public class SaveEpisodeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("comicID"));
        String redirectURL;
        if (request.getParameter("delete").equals("true")) {
            Series s = null;
            Episode ep = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", id).first().now();
            if (ep != null) {
                s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", ep.getSeriesID()).first().now();
                if (s != null) {
                    s.getEpisodeList().remove(s.getEpisodeList().indexOf(id));
                    ObjectifyService.ofy().save().entity(s).now();
                }
            }
            ObjectifyService.ofy().delete().type(Episode.class).id(id);
            if (s != null) {
                RequestDispatcher jsp = request.getRequestDispatcher("/series/" + s.getSeriesID().toString());
                jsp.forward(request,response);
            }
            else {
                redirectURL = "/manageAccount.jsp";
                response.sendRedirect(redirectURL);
            }
        }
        else {
            String title = request.getParameter("title");
            String desc = request.getParameter("description");
            String hashtag = request.getParameter("hashtag");
            Episode episode = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", id).first().now();


            episode.setTitle(title);
            episode.setDescription(desc);
            episode.setHashtagList(hashtag);
            ObjectifyService.ofy().save().entity(episode).now();

            redirectURL = "/view/" + episode.getComicID();
            response.sendRedirect(redirectURL);
        }
    }
}
