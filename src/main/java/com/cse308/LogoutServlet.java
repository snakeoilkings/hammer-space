package com.cse308;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by kATHRYN on 4/5/2016.
 */
public class LogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Clears all session objects */
        HttpSession session = request.getSession();
        if(session!=null)
            session.invalidate();

        /* Clears request objects that identify user */
        request.setAttribute("account", null);
        request.setAttribute("user", null);

        RequestDispatcher jsp = request.getRequestDispatcher("./login");
        jsp.forward(request, response);
    }
}
