package com.cse308;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;
import entity.Series;
import handler.ComicBean;

/**
 * Created by kATHRYN on 4/26/2016.
 */
public class CreateEpisodeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

       if (session.getAttribute("comicID")==null) {

            Series newSeries = new Series("This Pic I Made", (String)session.getAttribute("account"));
            ObjectifyService.ofy().save().entity(newSeries).now();
            Long num = newSeries.getSeriesID();

            Episode newEp = new Episode();
            newEp.setSeriesID(num);
            ObjectifyService.ofy().save().entity(newEp).now();
            session.setAttribute("comicID", newEp.getComicID());
            session.setAttribute("comic", null);
        }
        else {
            Episode comic = ObjectifyService.ofy().load().type(Episode.class).filter("comicID",session.getAttribute("comicID")).first().now();
            ComicBean bean = new ComicBean();
            bean.setId((Long)session.getAttribute("comicID"));
            bean.setSave(comic.getTempSave());
            session.setAttribute("comic", bean);
        }

        RequestDispatcher jsp = request.getRequestDispatcher("/draw.jsp");
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
