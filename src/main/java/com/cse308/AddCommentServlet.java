package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Episode;
import entity.Series;
import entity.Comment;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 5/4/16.
 */
public class AddCommentServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String comicURL = request.getParameter("comicIDForm");
        String username = request.getParameter("accountForm");

        Long comicID = Long.parseLong(comicURL);

        Episode ep = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", comicID).first().now();
        Account act = ObjectifyService.ofy().load().type(Account.class).filter("username", username).first().now();

        Comment comment = new Comment(request.getParameter("commentForm"), username, Long.parseLong(comicURL));

        act.addComment(comment);
        ep.addComment(comment);

        ObjectifyService.ofy().save().entity(act).now();
        ObjectifyService.ofy().save().entity(ep).now();

        RequestDispatcher jsp = request.getRequestDispatcher("/view/" + comicID);
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
