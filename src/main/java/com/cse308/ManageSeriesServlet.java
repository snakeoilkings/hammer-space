package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Series;
import entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 4/28/2016.
 */
public class ManageSeriesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.valueOf(request.getParameter("seriesID"));
        Series series = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", id).first().now();
        request.setAttribute("seriesID", id);
        request.setAttribute("title", series.getTitle());
        request.setAttribute("description", series.description);
        request.setAttribute("category", series.category);

        RequestDispatcher jsp = request.getRequestDispatcher("/manageSeries.jsp");
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
