package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 4/28/2016.
 */
public class ManageEpisodeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.valueOf(request.getParameter("comicID"));
        Episode episode = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", id).first().now();
        request.setAttribute("comicID", id);
        request.setAttribute("title", episode.getTitle());
        request.setAttribute("description", episode.getDescription());
        request.setAttribute("hashtag", episode.getHashtagListAsString());

        RequestDispatcher jsp = request.getRequestDispatcher("/manageEpisode.jsp");
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
