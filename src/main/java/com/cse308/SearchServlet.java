package com.cse308;

import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import entity.Episode;
import entity.Series;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kATHRYN on 5/16/2016.
 */
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchType = request.getParameter("search_type");
        String searchParameters = request.getParameter("search_params").toLowerCase();
        boolean comicReturn = false;

        // determines if we are returning episode objects or series objects
        if (searchType.equals("episode") || searchType.equals("hashtag") || searchType.equals("all")) {
            comicReturn = true;
        }

        String[] words = searchParameters.split(" ");

        if (comicReturn) {
            ArrayList<Episode> matches = new ArrayList();
            ArrayList<Episode> nearMatches = new ArrayList();
            /* look for episode title */
            if (searchType.equals("episode") || searchType.equals("all")) {
                Query<Episode> q = ObjectifyService.ofy()
                        .load()
                        .type(Episode.class);
                QueryResultIterator<Episode> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Episode ep = iterator.next();
                    if (ep.getTitle()!= null && ep.getTitle().toLowerCase().contains(searchParameters)) {
                        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", ep.getSeriesID()).first().now();
                        if (s != null)
                            ep.setStitle(s.getTitle());
                        matches.add(ep);
                    }
                    for (String word : words) {
                        if (ep.getTitle() != null && ep.getTitle().toLowerCase().contains(word) && !nearMatches.contains(ep) && !matches.contains(ep)) {
                            Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", ep.getSeriesID()).first().now();
                            if (s != null)
                                ep.setStitle(s.getTitle());
                            nearMatches.add(ep);
                        }
                    }
                }
            }
            /* look for episode title */
            if (searchType.equals("hashtag") || searchType.equals("all")) {
                Query<Episode> q = ObjectifyService.ofy()
                        .load()
                        .type(Episode.class);
                QueryResultIterator<Episode> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Episode ep = iterator.next();
                    if (ep.getHashtagListAsString() != null && ep.getHashtagListAsString().toLowerCase().contains(searchParameters)) {
                        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", ep.getSeriesID()).first().now();
                        if (s != null)
                            ep.setStitle(s.getTitle());
                        matches.add(ep);
                    }
                    for (String word : words) {
                        if (ep.getHashtagListAsString() != null && ep.getHashtagListAsString().toLowerCase().contains(word) && !nearMatches.contains(ep) && !matches.contains(ep)) {
                            Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", ep.getSeriesID()).first().now();
                            if (s != null)
                                ep.setStitle(s.getTitle());
                            nearMatches.add(ep);
                        }
                    }
                }
            }
            request.setAttribute("comicResults", matches);
            request.setAttribute("nearComicResults", nearMatches);
        }

        /* return series objects */
        if (!comicReturn || searchType.equals("all")) {
            ArrayList<Series> smatches = new ArrayList();
            ArrayList<Series> snearMatches = new ArrayList();
            /* look for series name*/
            if (searchType.equals("series") || searchType.equals("all")) {
                Query<Series> q = ObjectifyService.ofy()
                        .load()
                        .type(Series.class);
                QueryResultIterator<Series> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Series s = iterator.next();
                    if (s.getTitle()!= null && s.getTitle().toLowerCase().contains(searchParameters)&& !snearMatches.contains(s) && !smatches.contains(s)) {
                        smatches.add(s);
                    }
                    for (String word : words) {
                        if (s.getTitle() != null && s.getTitle().toLowerCase().contains(word) && !snearMatches.contains(s) && !smatches.contains(s))
                            snearMatches.add(s);
                    }
                }
            }
            /* look for author */
            if (searchType.equals("author") || searchType.equals("all")) {
                Query<Series> q = ObjectifyService.ofy()
                        .load()
                        .type(Series.class);
                QueryResultIterator<Series> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Series s = iterator.next();
                    if (s.getAuthor()!= null && s.getAuthor().toLowerCase().contains(searchParameters)&& !snearMatches.contains(s) && !smatches.contains(s)) {
                        smatches.add(s);
                    }
                    for (String word : words) {
                        if (s.getAuthor() != null && s.getAuthor().toLowerCase().contains(word) && !snearMatches.contains(s) && !smatches.contains(s))
                            snearMatches.add(s);
                    }
                }
            }
            /* look for category */
            if (searchType.equals("category") || searchType.equals("all")) {
                Query<Series> q = ObjectifyService.ofy()
                        .load()
                        .type(Series.class);
                QueryResultIterator<Series> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Series s = iterator.next();
                    if (s.getCategory()!= null && s.getCategory().toLowerCase().contains(searchParameters)&& !snearMatches.contains(s) && !smatches.contains(s)) {
                        smatches.add(s);
                    }
                    for (String word : words) {
                        if (s.getCategory() != null && s.getCategory().toLowerCase().contains(word) && !snearMatches.contains(s) && !smatches.contains(s))
                            snearMatches.add(s);
                    }
                }
            }
            /* look for description */
            if (searchType.equals("description") || searchType.equals("all")) {
                Query<Series> q = ObjectifyService.ofy()
                        .load()
                        .type(Series.class);
                QueryResultIterator<Series> iterator = q.iterator();
                while (iterator.hasNext()) {
                    Series s = iterator.next();
                    if (s.getDescription()!= null && s.getDescription().toLowerCase().contains(searchParameters)&& !snearMatches.contains(s) && !smatches.contains(s)) {
                        smatches.add(s);
                    }
                    for (String word : words) {
                        if (s.getDescription() != null && s.getDescription().toLowerCase().contains(word) && !snearMatches.contains(s) && !smatches.contains(s))
                            snearMatches.add(s);
                    }
                }
            }
            request.setAttribute("seriesResults", smatches);
            request.setAttribute("nearSeriesResults", snearMatches);
        }

        RequestDispatcher jsp = request.getRequestDispatcher("/searchResults.jsp");
        jsp.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
