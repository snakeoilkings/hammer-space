package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by kATHRYN on 4/5/2016.
 */
public class ValidateNameServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String test = request.getParameter("username");
        PrintWriter out = response.getWriter();
        boolean valid = validateUser(test);
        if (valid)
            out.write("success");
        else
            out.write("failure");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public static boolean validateUser(String username) {
        Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", username).first().now();
        if (a == null)
            return true;
        else
            return false;
    }

    public static boolean isAlphaNumeric(String user) {

        for (int i=0; i < user.length(); i++) {
            Character c = user.charAt(i);
            if (!Character.isLetterOrDigit(c))
                if (!(c == '_') && !(c =='-'))
                    return false;
        }
        return true;
    }
}
