package com.cse308;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Series;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by kATHRYN on 4/29/2016.
 */
public class BuildSeriesServlet extends HttpServlet {

    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Series newSeries = new Series();

        String author = request.getParameter("author");
        newSeries.setAuthor(author);
        newSeries.setDescription(request.getParameter("description"));
        newSeries.setTitle(request.getParameter("title"));
        newSeries.setCategory(request.getParameter("category"));

        Map<String, List<BlobKey>> blobs = this.blobstoreService.getUploads(request);
        List<BlobKey> blobKeys = blobs.get("file");

        if (blobKeys != null)
            newSeries.setSeriesPic(blobKeys.get(0).getKeyString());

        ObjectifyService.ofy().save().entity(newSeries).now();

        /* open account entity and store new series */
        Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", author).first().now();
        a.addSeries(newSeries.getSeriesID());
        ObjectifyService.ofy().save().entity(a).now();

        request.setAttribute("seriesID", newSeries.getSeriesID());

        request.getRequestDispatcher("/buildepisode.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
