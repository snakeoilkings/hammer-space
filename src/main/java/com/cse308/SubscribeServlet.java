package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Episode;
import entity.Series;
import handler.SubBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kATHRYN on 4/28/2016.
 */
public class SubscribeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action.equals("subscribe")) {
            String account = request.getParameter("username");
            Long seriesID = Long.parseLong(request.getParameter("seriesID"));

            Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", account).first().now();
            Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", seriesID).first().now();
            a.addSubscription(seriesID);
            ObjectifyService.ofy().save().entity(a).now();

            ArrayList<SubBean> subscrips = (ArrayList<SubBean>)request.getSession().getAttribute("subs");
            SubBean b = new SubBean();
            b.setId(seriesID);
            b.setTitle(s.getTitle());
            subscrips.add(b);
            request.getSession().setAttribute("subs", subscrips);
        }
        if (action.equals("like")) {
            Long comicID = Long.parseLong(request.getParameter("comicID"));
            Episode c = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", comicID).first().now();
            c.setLike();
            ObjectifyService.ofy().save().entity(c).now();
        }

        if (action.equals("recent")) {
            HttpSession session = request.getSession();
            if (session.getAttribute("account") != null) {
                Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", session.getAttribute("account")).first().now();
                a.addRecentlyComicList(Long.parseLong(request.getParameter("seriesID")));
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
