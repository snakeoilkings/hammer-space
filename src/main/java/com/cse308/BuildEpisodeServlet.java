package com.cse308;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Episode;
import entity.Series;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by kATHRYN on 4/29/2016.
 */
public class BuildEpisodeServlet extends HttpServlet {
    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Episode newEp = new Episode();

        Long series = Long.valueOf(request.getParameter("seriesID"));
        newEp.setAuthor(request.getParameter("author"));
        newEp.setDescription(request.getParameter("description"));
        newEp.setTitle(request.getParameter("title"));
        newEp.setSeriesID(series);
        newEp.setHashtagList(request.getParameter("hashtags"));


        Map<String, List<BlobKey>> blobs = this.blobstoreService.getUploads(request);
        List<BlobKey> blobKeys = blobs.get("file");

        if (blobKeys != null) {
            newEp.setUrlString("/serve?blob-key=" + blobKeys.get(0).getKeyString());
            newEp.setFinished(true);
        }
        else {
            newEp.setFinished(false);
        }

        ObjectifyService.ofy().save().entity(newEp).now();

        /* open series entity and store new episode */
        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", series).first().now();
        s.addEpisode(newEp.getComicID());
        ObjectifyService.ofy().save().entity(s).now();

        String redirectURL;
        if (request.getParameter("buildnew").equals("false")) {
            redirectURL = "/view/" + newEp.getComicID();
            response.sendRedirect(redirectURL);
        }
        else {
            request.setAttribute("comicID", newEp.getComicID());
            RequestDispatcher jsp = request.getRequestDispatcher("/draw.jsp");
            jsp.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
