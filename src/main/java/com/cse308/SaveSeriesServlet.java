package com.cse308;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Episode;
import entity.Series;
import handler.SeriesBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kATHRYN on 5/4/2016.
 */
public class SaveSeriesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        Long id = Long.parseLong(request.getParameter("seriesID"));
        Series series = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", id).first().now();
        String redirectURL;
        if (request.getParameter("delete").equals("true")) {
            //removing series. then should remove all of its episodes as well.
            for(Long n :series.getEpisodeList()){
                Episode ep = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", n).first().now();
                if(ep != null) {
                    ObjectifyService.ofy().delete().type(Episode.class).id(n);
                }
            }
            ObjectifyService.ofy().delete().type(Series.class).id(id);

            Account a = ObjectifyService.ofy().load().type(Account.class).filter("username", series.getAuthor()).first().now();
            a.removeSeries(id);
            ObjectifyService.ofy().save().entity(a).now();

            //Making changes to the bean
            ArrayList<SeriesBean> seriesb = (ArrayList<SeriesBean>) request.getSession().getAttribute("series");
            int index = -1;
            for(SeriesBean s : seriesb){
                if(s.getId() == id){
                    index = seriesb.indexOf(s);
                }
            }
            if(index > 0){
                seriesb.remove(index);
            }
            request.getSession().setAttribute("series", seriesb);
            redirectURL = "/";
        }else {

            String title = request.getParameter("title");
            String desc = request.getParameter("description");
            String category = request.getParameter("category");
            series.setTitle(title);
            series.setDescription(desc);
            series.setCategory(category);

            //Set series pic
            Map<String, List<BlobKey>> blobs = BlobstoreServiceFactory.getBlobstoreService().getUploads(request);
            List<BlobKey> blobKeys = blobs.get("file");

            if (blobKeys != null)
                series.setSeriesPic(blobKeys.get(0).getKeyString());

            //save to datastore
            ObjectifyService.ofy().save().entity(series).now();
            redirectURL = "/series/" + series.getSeriesID();
        }
        response.sendRedirect(redirectURL);
    }
}
