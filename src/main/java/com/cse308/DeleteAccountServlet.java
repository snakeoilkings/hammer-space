package com.cse308;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.User;

/**
 * Created by kATHRYN on 4/21/2016.
 */
public class DeleteAccountServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserService userService = UserServiceFactory.getUserService();
        com.google.appengine.api.users.User user = userService.getCurrentUser();
        String logoutUrl = userService.createLogoutURL("/logout");

        User usr = ObjectifyService.ofy().load().type(User.class).filter("username", request.getSession().getAttribute("account")).first().now();
        Account accnt = ObjectifyService.ofy().load().type(Account.class).filter("username", request.getSession().getAttribute("account")).first().now();

        ObjectifyService.ofy().delete().entity(usr).now();
        ObjectifyService.ofy().delete().entity(accnt).now();

        response.sendRedirect(logoutUrl);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
