package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 5/14/2016.
 */
public class DrawServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.valueOf(request.getParameter("comicID"));
        Episode episode = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", id).first().now();

        request.setAttribute("comic", episode);

        RequestDispatcher jsp = request.getRequestDispatcher("/draw.jsp");
        jsp.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
