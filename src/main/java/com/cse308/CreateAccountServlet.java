package com.cse308;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.cse308.ValidateNameServlet;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.Account;

/**
 * Created by kATHRYN on 4/5/2016.
 */
public class CreateAccountServlet extends HttpServlet {
    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* backend validation of username info .*/
        String name = request.getParameter("username");
        if (name.length() < 4 || !ValidateNameServlet.validateUser(name) || !ValidateNameServlet.isAlphaNumeric(name)) {
            response.sendRedirect("/login");
        } else {
            Map<String, List<BlobKey>> blobs = this.blobstoreService.getUploads(request);
            List<BlobKey> blobKeys = blobs.get("myFile");

            HttpSession session = request.getSession();
            Object user = session.getAttribute("user");

        /* no profile pic selected */
            if (blobKeys == null) {
                entity.User newUser = new entity.User(request.getParameter("username"), (String) user);
                ObjectifyService.ofy().save().entity(newUser).now();
            }
        /* profile pic added to user object */
            else {
                entity.User newUser = new entity.User(request.getParameter("username"), (String) user, blobKeys.get(0).getKeyString());
                ObjectifyService.ofy().save().entity(newUser).now();
                session.setAttribute("blobkey", blobKeys.get(0).getKeyString());
            }

        /* create account */
            Account newAccount = new Account(request.getParameter("username"));
            ObjectifyService.ofy().save().entity(newAccount).now();

            request.setAttribute("account", request.getParameter("username"));
            response.sendRedirect("/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}


