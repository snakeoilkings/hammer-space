package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Episode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kATHRYN on 4/27/2016.
 */
public class PublishServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String saved = request.getParameter("drawing");
        Long id = Long.parseLong(request.getParameter("comicID"));

        Episode ep = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", id).first().now();

        if (ep != null) {
            ep.setUrlString(saved);
            ep.setFinished(true);
            ObjectifyService.ofy().save().entity(ep).now();
        }

        if (ep==null) {

        }
        response.sendRedirect("/view/" + request.getParameter("comicID"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
