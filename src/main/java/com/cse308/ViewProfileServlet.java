package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import entity.Account;
import entity.Series;
import entity.User;
import handler.SeriesBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kATHRYN on 5/1/2016.
 */
public class ViewProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURL().toString();
        String[] urlParts = url.split("/");
        String username = urlParts[urlParts.length-1];

        User usr = ObjectifyService.ofy().load().type(User.class).filter("username", username).first().now();

        if (usr != null) {
            Account act = ObjectifyService.ofy().load().type(Account.class).filter("username", username).first().now();

        /* dissect series list for relevant information */
            ArrayList<Long> seriesList = act.getSeriesList();
            ArrayList<SeriesBean> series = new ArrayList();

            for (Long id : seriesList) {
                Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", id).first().now();
                SeriesBean t = new SeriesBean();

                if (s.getSeriesPic() != null)
                    t.setPic("/serve?blob-key="+s.getSeriesPic());
                else
                    t.setPic(null);
                t.setId(id);
                t.setLink("/series/" + id.toString());
                t.setName(s.getTitle());
                series.add(t);
            }
            request.setAttribute("name", username);
            request.setAttribute("email", usr.getEmail());
            request.setAttribute("blobkey", usr.getProfilePic());
            request.setAttribute("series", series);
        }
        else {
            ArrayList<SeriesBean> series = new ArrayList();
            request.setAttribute("blobkey", null);
            request.setAttribute("name", "ERROR: User Does Not Exist");
            request.setAttribute("email", "No user " + username + " exists on this server.");
            request.setAttribute("series", series);
        }

        RequestDispatcher jsp = request.getRequestDispatcher("/viewprofile.jsp");
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
