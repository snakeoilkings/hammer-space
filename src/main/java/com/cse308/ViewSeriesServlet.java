package com.cse308;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import entity.Episode;
import entity.Series;
import handler.SeriesBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kATHRYN on 5/1/2016.
 */
public class ViewSeriesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURL().toString();
        String[] urlParts = url.split("/");
        Long seriesID = Long.parseLong(urlParts[urlParts.length-1]);
        request.setAttribute("seriesID", seriesID);
        ArrayList<Episode> eps = new ArrayList();

        Series s = ObjectifyService.ofy().load().type(Series.class).filter("seriesID", seriesID).first().now();
        SeriesBean t = new SeriesBean();
        t.setName(s.getTitle());
        t.setAuthor(s.getAuthor());
        t.setPic("/serve?blob-key=" + s.getSeriesPic());
        t.setDescription(s.getDescription());

        for(int i=0; i < s.getEpisodeList().size(); i++){
            Episode e = ObjectifyService.ofy().load().type(Episode.class).filter("comicID", s.getEpisodeList().get(i)).first().now();
            eps.add(e);
        }

        t.setEpisodes(eps);
        request.setAttribute("series", t);

        RequestDispatcher jsp = request.getRequestDispatcher("/viewSeries.jsp");
        jsp.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
