package com.cse308;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import entity.*;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by kATHRYN on 4/5/2016.
 */
public class OfyHelper implements ServletContextListener {
    public void contextInitialized(ServletContextEvent event) {
        ObjectifyService.register(User.class);
        ObjectifyService.register(Account.class);
        ObjectifyService.register(Episode.class);
        ObjectifyService.register(Series.class);
        ObjectifyService.register(Comment.class);
    }

    public void contextDestroyed(ServletContextEvent event) {

    }

}
