package entity;

/**
 * Created by Yeseul on 4/5/2016.
 */

import com.google.appengine.api.datastore.DatastoreService;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Series:
 - KEY: seriesID
 - title
 - author
 - episodeList
 - description
 */
@Entity
public class Series implements Serializable {
    @Id Long seriesID;
    @Index String title;
    @Index String author;
    @Index String seriesPic;
    public String description;
    public String category;
    public ArrayList<Long> episodeList = new ArrayList<>();

    public Series() {}

    public Series(String title, String author) {
        this.title = title;
        this.author = author;
        seriesPic=null;
        this.seriesID = null;
        this.description = "";
    }

    public Series(String title, String author, String seriesPic) {
        this.title = title;
        this.author = author;
        this.seriesPic = seriesPic;
        this.seriesID = null;
        this.description = "";
    }

    public void addEpisode(long comicId) {
        episodeList.add(comicId);
    }


    public void removeEpisode(Episode episode) {
        int toBeRemoved = episodeList.indexOf(episode.getComicID());
        episodeList.remove(toBeRemoved);
    }

    public String getTitle() {
        return title;
    }

    public String getSeriesPic() {
        return seriesPic;
    }

    public void setDescription(String desc){
        if(desc == null){
            return;
        }
        this.description = desc;
    }


    public ArrayList<Long> getEpisodeList() { return episodeList; }

    public String getAuthor() { return author; }

    public void setAuthor(String author) { this.author = author; }

    public Long getSeriesID() { return seriesID; }

    public void setTitle(String title) { this.title = title; }

    public void setSeriesPic(String seriesPic) { this.seriesPic = seriesPic; }

    public void setCategory(String category) { this.category = category; }

    public String getCategory() { return category; }

    public String getDescription(){ return description; }
}
