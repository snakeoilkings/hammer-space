package entity;

import com.google.appengine.repackaged.org.joda.time.LocalDateTime;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yeseul on 4/5/2016.
 */
@Embed
@Entity

public class Comment implements Serializable {
    @Id Long id;
    String comment;
    Date time;

    String author;
    Long comic;
    int numLikes;

    public Comment(){

    }

    public Comment(String comment, String author, Long comic) {
        this.comment = comment;
        this.author = author;
        this.comic = comic;
        numLikes = 0;
        time = new Date();
    }


    public String getAuthor() {
        return author;
    }

    public Long getComic() {
        return comic;
    }

    public int getNumLikes() {
        return numLikes;
    }

    public Date getTime() {
        return time;
    }

    public String getComment() {
        return comment;
    }

    public Long getId() {
        return id;
    }
}