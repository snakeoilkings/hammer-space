package entity;

/**
 * Created by Kathryn on 4/5/2016.
 */

import com.google.appengine.api.blobstore.BlobKey;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.io.Serializable;
import java.lang.String;

/**
 *
 **/
@Entity
public class User implements Serializable  {
    @Id
    String username;
    @Index
    String email;
    boolean permissions;
    String profilePic;

    public User() {
    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
        permissions = false;
        profilePic = null;
    }

    public User(String username, String email, String pic) {
        this.username = username;
        this.email = email;
        permissions = false;
        this.profilePic = pic;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getUsername() {
        return username;
    }

    public void setProfilePic(String pic) {
        profilePic = pic;
    }

    public void setEmail(String mail) {
        email = mail;
    }

    public String getEmail() { return email; }
}
