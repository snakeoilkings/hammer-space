package entity;

import com.google.appengine.repackaged.org.joda.time.LocalDateTime;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.io.Serializable;
import java.util.ArrayList;
import entity.Comment;
/**
 * Created by Yeseul on 4/5/2016.
 */


/**
 * Account:
 - KEY: username
 - subscriptionList
 - recentlyComicList (one that's recently viewd)
 - commentList
 - seriesList
 - notificationList
 */



@Entity
public class Account implements Serializable {
    @Id public String username;
    // a list of seriesID of the series subscribed to.
    public ArrayList<Long> subscriptionList = new ArrayList<>();
    // a list of comicID of the comics viewed recently.
    public ArrayList<Long> recentlyComicList = new ArrayList<>();
    // a list of comments
    public ArrayList<Comment> commentList = new ArrayList<>();
    // a list of series the user made.
    public ArrayList<Long> seriesList = new ArrayList<>();
    //TODO: need to add notification class.

    public Account() {}

    public Account(String username) {
        this.username = username;
    }

    public String getUsername() { return username; }

    public void addSeries(Long series) {
        seriesList.add(series);
    }

    public void removeSeries(Long series) {seriesList.remove(seriesList.indexOf(series));}

    public void addSubscription(Long seriesID) {
        if (!subscriptionList.contains(seriesID))
            subscriptionList.add(seriesID);
    }

    public void removeSubscription(Long subID) {
        for(int i=0; i<subscriptionList.size(); i++) {
            if (subscriptionList.get(i).longValue() == subID.longValue()) {
                subscriptionList.remove(i);
            }
        }
    }
    public ArrayList<Long> getSeriesList() { return seriesList; }

    public ArrayList<Comment> getCommentList() {
        return commentList;
    }

    public void addComment(Comment comment) {
        commentList.add(0, comment);
    }

    /* Stores Series ID's for recent comics to view on the homepage */
    public void addRecentlyComicList(Long seriesID) {
        recentlyComicList.add(seriesID);
        if (recentlyComicList.size() > 35)
            recentlyComicList.remove(0);
    }
}
