package entity;

/**
 * Created by Yeseul on 4/5/2016.
 */

//import com.google.appengine.repackaged.com.google.api.client.util.Key;
import com.google.appengine.repackaged.org.joda.time.LocalDateTime;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import entity.Comment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Episode:
 - KEY: comicID
 - title
 - author
 - seriesID
 - hashtagList
 - description
 -comments****
 */


@Entity
public class Episode implements Serializable {
    @Id Long comicID;
    @Index String title;
    @Index String author;
    @Index Long seriesID;
    @Index int likes;
    // a list of hashtags on the episode.
    ArrayList<String> hashtagList = new ArrayList<>();
    String description;
    // a list of comments on the episode.
    ArrayList<Comment> commentList = new ArrayList<>();
    String tempSave;
    String urlString;
    String stitle;
    boolean finished;

    public Episode() { }

    public Episode(String title, String author, String urlString, Long seriesID) {
        this.title = title;
        this.author = author;
        this.urlString = urlString;
        this.seriesID = seriesID;
        this.tempSave = null;
        this.comicID = null;
    }

    public Episode(String title, String author, String urlString, Long seriesID, String rawSave) {
        this.title = title;
        this.author = author;
        this.urlString = urlString;
        this.seriesID = seriesID;
        this.tempSave = rawSave;
        this.comicID = null;
    }

    public void setAuthor(String author) { this.author = author; }

    /* these values are not saved to entity */
    public void setStitle(String stitle) { this.stitle = stitle; }

    public String getStitle() { return stitle; }
    /* /not saved to entity */

    public void setDescription(String description) { this.description = description; }

    public void setHashtagList(String str){
        resetHashtagList();
        String pattern = "#";
        Pattern p = Pattern.compile(pattern);
        String[] list = p.split(str);
        for(String e: list){
            System.out.println(e);
            hashtagList.add(e);
        }

    }

    public void addHashtag(String tag){
        hashtagList.add(tag);
    }

    public void resetHashtagList(){
        hashtagList.clear();
    }

    public String getHashtagListAsString(){
        String s = "";
        for(String each: hashtagList){
            s += "#" + each + " ";
        }
        s += " ";
        return s;
    }

    public long getComicID() {
        return comicID;
    }

    public void setTempSave(String tempSave) {
        this.tempSave = tempSave;
    }

    public String getTempSave() {
        return tempSave;
    }

    public String getUrlString() { return urlString; }

    public String getDescription(){ return description;}

    public Long getSeriesID() { return seriesID; }

    public void setSeriesID(Long seriesID) { this.seriesID = seriesID; }

    public void setUrlString(String urlString) { this.urlString = urlString; }

    public void setTitle(String title) { this.title = title; }

    public String getTitle() { return title; }

    public int getLikes() { return likes; }

    public void setLike() { this.likes+=1; }

    public ArrayList<Comment> getCommentList() {
        return commentList;
    }

    public void addComment(Comment comment){
        commentList.add(0, comment);
    }

    public void setFinished(boolean fin) { finished = fin; }

    public boolean getFinished() { return finished; }
}
