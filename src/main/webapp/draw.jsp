<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Draw Your Episode</title>
    <%@include file="/head.html" %>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link href="/resources/literallycanvas/_assets/literallycanvas.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <style type="text/css">
        .fs-container {
            width: 800px;
            margin: auto;
        }
    </style>

</head>

<%@include file="header.jsp"%>

<br>
<br>
<br>

<div class="container">
    <br>
    <div class="papers text-center">
        <div class="heading text-center">
            <br/>
            <br/>
            <img class="dividerline" src="/img/sep.png" alt="">
            <h4>Build a New Series</h4>
            <img class="dividerline" src="/img/sep.png" alt="">
        </div>

        <div class="fs-container">
            <div id="lc"></div>
        </div>

        <input id="savedpic" type="hidden" value='${comic.tempSave}' />

        <script src="/resources/literallycanvas/_js_libs/react-0.14.3.js"></script>
        <script src="/resources/literallycanvas/_js_libs/literallycanvas.js"></script>

        <script>
        var lc = LC.init(document.getElementById("lc"), {
            imageURLPrefix: '/resources/literallycanvas/_assets/lc-images',
            toolbarPosition: 'bottom',
            defaultStrokeWidth: 2,
            strokeWidths: [1, 2, 3, 5, 30]
        });

        var localStorageKey = 'drawing';
        localStorage.setItem(localStorageKey, $('#savedpic').val());

        if (localStorage.getItem(localStorageKey)) {
            lc.loadSnapshot(JSON.parse(localStorage.getItem(localStorageKey)));
        }


        /*  DONT DELETE, keeping for intermittent saves to be implemented later
         TODO: set up intermittent saves here
         function save() {
         var id = $('#cid').val();
         var pic = JSON.stringify(lc.getSnapshot());

         $.ajax({
         type: "POST",
         url: "/save",
         data: {
         drawing: pic,
         comicID: id
         },
         success: function () {
         console.log("reached servlet");
         },
         error: function () {
         console.log("failure to contact servlet");
         },
         async: false
         });
         }*/

        function save() {
            var pic = JSON.stringify(lc.getSnapshot());
            $('#raw').val(pic);
        }

        function publish(){
            var pic = lc.getImage().toDataURL();
            var clean_save = document.getElementById("clean");
            clean_save.value=pic;
        }

        </script>
        <br>
        <br>
        <div class="text-center">
        <form action="/save" method="post" onsubmit="save()" style="display: inline-block;">
            <input name='comicID' type='hidden' value='<c:out value="${comicID}" default="${comic.comicID}"/>'/>
            <input name='drawing' id = 'raw' type='hidden' value=''/>
            <input type="submit" style="width: 270px;" class="generic_button" value='save and edit later'>
        </form>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <form action="/publish" method="post" onsubmit="publish()" style="display: inline-block;">
            <input name='comicID' type='hidden' value='<c:out value="${comicID}" default="${comic.comicID}"/>'/>
            <input name='drawing' id = 'clean' type='hidden' value=''/>
            <input type="submit" style="width: 270px;" class="generic_button" value='publish'>
        </form>
    </div>
    </div>
</div>

<%@ include file="footer.jsp"%>
</body>
</html>