<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/12/16
  Time: 10:23 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Account Management</title>
    <%@include file="/head.html" %>

    <script>
        function new_ep(id) {
            var lists = document.getElementsByName("seriesID");
            for (var i in lists) {
                i.value = id;
            }
        }
    </script>

</head>

<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

    <c:choose>
        <c:when test="${(user != null) && (account == null)}">
            <div class="container">
                <%@include file="/accountSetup.jsp" %>
            </div>
        </c:when>
    <c:otherwise>

    <div class="container">
        <br>
        <div class="papers text-center">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4>Account Settings</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>

        <form id="ManageAccount" method="post" action="<%=BlobstoreServiceFactory.getBlobstoreService().createUploadUrl("/manageAccount") %>" enctype="multipart/form-data" class="thumbnail">

            <fieldset>
                <h2>${sessionScope.account}</h2>
                <h5>Make changes to your profile picture and your subscriptions.</h5>
                <hr>
            </fieldset>
            <br/>
            <fieldset id = "changeImageBox" >
                <h4>Change Profile Image</h4>
                <br>
                    <input type="file"  id="myFile2" name="aFile">
            </fieldset>

            <br>
            <h4>Subscriptions</h4>

            <fieldset id = "subscriptionOption">
                <br>
                <div style = "display: inline-block; align: center;">
                    <table>
                        <c:if test="${empty subs}">No subscriptions yet!</c:if>
                        <c:forEach items="${subs}" var="sub">
                            <tr>
                                <td style = "padding-bottom: 15px;"><b>${sub.title}</b></td>
                                <td style = "padding-left: 50px; padding-bottom: 15px;"><input type="checkbox" name="unsub" value="${sub.id}"/>  Unsubscribe</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </fieldset>

            <br/>
            <br/>
            <br/>
            <button type="submit" id = "submitBtn" class="generic_button">Submit</button>
            <br>
            <br>
            <br>
            Click here to <a href = "/deleteaccount.jsp">delete account</a>
            <br/>
            <br/>
        </form>

    <br>

    <c:if test="${not empty sessionScope.series}">
        <div class = "thumbnail">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4>Manage My Series</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>
            <br/>
            <br/>

            <section class="gallery" id="gallery">
                <div class="container">
                    <div class="grid-gallery">
                        <section class="grid-wrap">
                            <ul class="grid">
                                <c:forEach var="s" items="${sessionScope.series}">
                                <li>
                                    <div class="img_gal">
                                        <a target="_blank" href="${s.link}">
                                            <img src="${s.pic}">
                                        </a>
                                        <div class="desc">${s.name}
                                            <form action="/buildepisode.jsp" method="post" onclick="">
                                                <input type="text" name = "seriesID" hidden="true" value="${s.id}">
                                                <input type="submit" value="Add a New Episode">
                                            </form>
                                        </div>
                                    </div>
                                </li>
                                </c:forEach>
                            </ul>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    </c:if>

            <div>
                <img class="dividerline" src="/img/sep.png" alt="">
                <a href = "/buildseries.jsp"><h3 id="startnew"> Start a new Series</h3></a>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>
        </div>
    </div>
</c:otherwise>
</c:choose>
</div>

<%@include file="footer.jsp"%>


</body>
</html>
