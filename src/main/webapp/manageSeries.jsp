<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/26/2016
  Time: 4:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Series Settings</title>
    <%@include file="/head.html" %>

    <script>
        function delete1() {
            $("#del").submit();
        }

        function deleteseries(){
            var series=${series};
            $.ajax({
                type: "POST",
                url: "/manage_series",
                data: {
                    delete: true,
                    seriesID: series
                },
                success: function () {
                    console.log("reached servlet");
                },
                error: function () {
                    console.log("failure to contact servlet");
                }
            });
            document.getElementById("published_pic").setAttribute()
        }
    </script>
</head>

<body>
<div class="wrapper" id="wrapper">
<%@include file="/header.jsp"%>

<c:choose>
    <c:when test="${(user != null) && (account == null)}">
        <div class="container">
            <%@include file="/accountSetup.jsp" %>
        </div>
    </c:when>
    <c:otherwise>

        <div class="container">
            <br>
            <div class="papers text-center">
                <div class="heading text-center">
                    <br/>
                    <br/>
                    <br/>
                    <img class="dividerline" src="/img/sep.png" alt="">
                    <h4>Series Settings</h4>
                    <img class="dividerline" src="/img/sep.png" alt="">
                </div>
                <br>
                <br>
                <br>
                <form action="<%=BlobstoreServiceFactory.getBlobstoreService().createUploadUrl("/save_series") %>" method="post" enctype="multipart/form-data">
                    <input type="text" name="account" value="${sessionScope.account}" hidden="true"/>
                    <input type="text" name="seriesID" hidden="true" value="${seriesID}">
                    <label for="title">Series Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="${title}" required="true">
                    <br>
                    <label for="desc">Description</label>
                    <input type="text" class="form-control" id="desc" name="desc" value="${description}" required="true">
                    <br>
                    <label for="category">Category</label>
                    <input type="text" class="form-control" id="category" name="category" value="${category}" required="true">
                    <br>
                    <label for="file">Thumbnail Image</label>
                    <input type="file" class="form-control" id="file"  name="file">
                    <br>
                    <br>
                    <button type="submit" class="generic_button">Save</button>
                </form>
                <br><br><br>

                <br><br><br>

                Want to delete this series?
                <form method="post" action="/save_series" id="del">
                    <input type="text" name="delete" hidden="true" value="true">
                    <input type="text" name="seriesID" hidden="true" value="${seriesID}"> <a onclick="delete1();">click here</a>.
                </form>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
    <%@ include file="footer.jsp" %>
</div>
</body>
</html>
