<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 5/3/16
  Time: 7:28 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Search Results</title>
    <%@include file="/head.html" %>
</head>
<body>

<div class="wrapper" id="wrapper">

<%@include file="/header.jsp"%>

<div class="container">
    <br>
    <div class="text-center">
        <br>
        <br>
        <h3>Search results for "${param.search_params}"</h3>
        <br>
        <br/>
        <c:if test="${not empty requestScope.comicResults}">
            <h2 class = "SearchText"> Comic results </h2>
            <c:forEach var="comic" items="${requestScope.comicResults}">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a href="/view/${comic.comicID}">
                    <img src = "<c:out value="${comic.urlString}" default = "http://www.eltis.org/sites/eltis/files/default_images/photo_default_4.png"/>" >
                        <h2>${comic.title}</h2>
                        <h3>${comic.stitle}</h3>
                    </a>
                </div>
            </div>
            </c:forEach>
        </c:if>
        <br/>
        <c:if test="${not empty requestScope.nearComicResults}">
            <c:forEach var="comic" items="${requestScope.nearComicResults}">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <a href="/view/${comic.comicID}">
                        <img id = "seriesImgId" class = "seriesImg" src = "<c:out value="${comic.urlString}" default = "http://www.eltis.org/sites/eltis/files/default_images/photo_default_4.png"/>" >
                        <h2>${comic.title}</h2>
                        <h3>${comic.stitle}</h3>
                        </a>
                    </div>
                </div>
            </c:forEach>
        </c:if>
    <br>
    <br>
    </div>
</div>
<div class="container">
    <div class="text-center">
    <c:if test="${not empty requestScope.seriesResults}">
    <br>
        <h2 class="SearchText">Series Results</h2>
        <c:forEach var="comic" items="${requestScope.seriesResults}">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a href="/series/${comic.seriesID}">
                        <img src = "<c:out value="/serve?blob-key=${comic.seriesPic}" default = "http://www.eltis.org/sites/eltis/files/default_images/photo_default_4.png"/>" >
                        <div class="caption">
                            <h3>${comic.title}</h3>
                            <h2></h2>
                        </div>
                    </a>
                </div>
            </div>
        </c:forEach>
    </c:if>
    <br/>
    <c:if test="${not empty requestScope.nearSeriesResults}">
        <c:forEach var="comic" items="${requestScope.nearSeriesResults}">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a href="/series/${comic.seriesID}">
                    <img src = "<c:out value="/serve?blob-key=${comic.seriesPic}" default = "http://www.eltis.org/sites/eltis/files/default_images/photo_default_4.png"/>" >
                    <h3>${comic.title}</h3>
                    </a>
                </div>
            </div>
        </c:forEach>
    </c:if>
        <br>
        <br>
        <br>
        <br>
        </div>
    </div>
</div>

<%@include file="footer.jsp"%>
</body>
</html>
