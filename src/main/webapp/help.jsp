<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/12/16
  Time: 10:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Help</title>
    <%@include file="/head.html"%>
</head>

<body>

<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

    <div class="wid-wrapper">
    <section class="gallery" id="help">
        <div class="container">
            <br/>
            <br/>
            <h2>Help</h2>


    <!-- Nav tabs -->
    <div id = "navigation" class = "navigationTool">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign Up / Log In</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">How to Start your Story</a></li>
        </ul>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <br>
            <br>
            <br>
            <div id = "browsing2">
                <div class="panel-body browsing">
                    <h2 class = "browsingHead">How to Sign Up</h2>
                    <br>
                    <div class="well well-lg">
                        <h4>
                            You can sign up by clicking the sign up link and logging in with a google account. You will
                            be directed to an account setup page that must be completed to allow for browsing. Just
                            select a username and you can optionally upload a profile pic. If you'd like to change the
                            picture later, you can do so in that account management area.
                        </h4>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="panel-body browsing">
                    <h2 class = "browsingHead">How to Log in</h2>
                    <br>
                    <div class="well well-lg">
                        <h4>
                            A user can login to access all sorts of special features like subscribing, creating your own
                            comics, and more! Click login and select the email you signed up with, then you are ready to
                            use all of our sites unique features.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <br>
            <br>
            <br>
            <div class="panel-body browsing">
                <h2 class = "browsingHead">How to Browse Comics</h2>
                <br>
                <div class="well well-lg">
                    <h4>
                        Browsing comics is easy! You can find comics by the search bar or browse for new comics and your
                        latest subscriptions from the homepage. Discover new comics that you might enjoy, and also see
                        recent comics that you can pick back up on from here.
                    </h4>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="panel-body browsing">
                <h2 class = "browsingHead">How to Search Comics</h2>
                <br>
                <div class="well well-lg">
                     <h4>
                           To search comics, enter the information you'd like to search in the search bar, and select what
                           field you'd like to search. You can search by series name, author name, or by hashtags or description.
                     </h4>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="panel-body browsing">
                <h2 class = "browsingHead">How to Start a new Comic </h2>
                <br>
                <div class="well well-lg">
                    <h4>
                        To start a new comic, you need to add it to a current series you own, or start a new one. A
                        new series can be done from the home page, or from any page under the menu. You will need a title, at
                        least one hastag, and a description. You can also add a thumbnail for view on the homepage for other
                        users. These fields can be changed later on from the manage series area.
                    </h4>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="panel-body browsing">
                <h2 class = "browsingHead">How to Draw a new Comic </h2>
                <br>
                <div class="well well-lg">
                    <h4>
                        If you'd like to create right on the website, you can! First, start building your new comic by selecting
                        the series you are writing to, and then add a new comic. Then add info such as title, description, and
                        hashtags. After which you can choose to upload or create, choose create and you will be directed to
                        a canvas to work on. You can save your progress for a later date, or publish once you're done!
                    </h4>
               </div>
            </div>
            <br>
            <br>
            <br>
            <div class="panel-body browsing">
                <h2 class = "browsingHead">How to Upload a Comic </h2>
                <br>
                <div class="well well-lg">
                    <h4>
                        If you drew your comic off of the site and want to upload it, instead of choosing create after
                        starting your new comic, choose upload, and find the filepath on your computer. After inputting all
                        of the information and selecting the proper upload, click submit to add your new comic!
                    </h4>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
        </div>
    </section>
    </div>
</div>

style="margin-left:auto;margin-right:auto;width:960px;"

<%@include file="footer.jsp"%>
</body>
</html>
