<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/12/2016
  Time: 11:41 AM
  To change this template use File | Settings | File Templates.
--%>
<!-- <navigation bar> -->
<header>
    <div class="menu">
        <div class="navbar-wrapper">
            <div class="container">
                <div class="navwrapper">
                    <div class="navbar navbar-inverse navbar-static-top">
                        <div class="container">
                            <div class="navArea">
                                <div class="navbar-collapse collapse" style="display:inline-block;">
                                    <div class="heading" style="display:inline-block;">
                                        <h2><a href = "/">Hammerspace Comics</a></h2>
                                    </div>
                                    <div class="parallax-pattern-overlay" style="display:inline-block;padding-left: 70px">
                                        <div class="container text-center" style="display:inline-block;">
                                            <form action="/search" method="get" style="display: inline-block">
                                                <select id = "dropdown" name="search_type" style = "width: 120px; height: 29px;">
                                                    <option class = "dropMenu" value = "all" selected>ALL</option>
                                                    <option class = "dropMenu" value = "episode">Episode</option>
                                                    <option class = "dropMenu" value = "series">Series</option>
                                                    <option class = "dropMenu" value = "hashtag">Hashtag</option>
                                                    <option class = "dropMenu" value = "description">Description</option>
                                                    <option class = "dropMenu" value = "category">Category</option>
                                                    <option class = "dropMenu" value = "author">Author</option>
                                                </select>
                                                <input type="text" name="search_params">
                                                <input type="submit" onclick="searchBarFunc()" value="Search"/>
                                            </form>
                                        </div>
                                    </div>
                                    <ul class="nav navbar-nav" style="display:inline-block; float:right; margin-right: -40px;">
                                        <c:choose>
                                            <c:when test="${user == null}">
                                                <li class="menuItem"><a href="${loginUrl}">Login</a></li>
                                                <li class="menuItem"><a href="${loginUrl}">Sign Up</a></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="menuItem"><a href="/profile/${sessionScope.account}">${account}!</a></li>
                                                <li class="menuItem"><a href="/manageAccount.jsp">My Account</a></li>
                                                <li class="menuItem"><a href="${logoutUrl}">Log Out</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                        <li class="menuItem"><a href="/help.jsp">Help</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
