<%--
  Created by IntelliJ IDEA.
  User: kATHRYN
  Date: 4/6/2016
  Time: 6:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Profile Page</title>
    <%@include file="/head.html" %>
</head>
<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>
    <div class="container">
        <br>
        <div class="<c:if test="${sessionScope.account == name}">papers</c:if> text-center">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4><c:if test="${sessionScope.account == name}">My</c:if><c:if test="${sessionScope.account != name}">${name}'s</c:if> Profile</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>
            <br>
            <br>
            <c:if test="${empty blobkey}">
                <img src="https://lh3.googleusercontent.com/-Y2IJzlUUV70/AAAAAAAAAAI/AAAAAAAAAAA/HMC3aK1S30o/photo.jpg">
            </c:if>
            <c:if test="${not empty blobkey}">
                <img id = "profileImg" src="/serve?blob-key=${blobkey}">
            </c:if>
            <h3>${name}</h3>
            <h3>${email}</h3>
            <p>
                <c:if test="${sessionScope.account == name}">
                    <br>
                    <br>
                    <h3><a href="/manageAccount.jsp" class="btn generic_button" role="button">Change Profile Picture</a></h3>
                </c:if>
            </p>
            <br/>
            <br/>
            <br/>
            <c:if test="${not empty series}">
                <div class="heading text-center">
                    <img class="dividerline" src="/img/sep.png" alt="">
                    <h4>My Series</h4>
                    <img class="dividerline" src="/img/sep.png" alt="">
                    <br/>
                    <br/>
                    <c:forEach var="s" items="${series}">
                        <img id = "seriesImgId" class = "seriesImg" src = "<c:out value="${s.pic}" default = "http://www.eltis.org/sites/eltis/files/default_images/photo_default_4.png"/>" >
                        <div class="caption">
                            <h3>${s.name}</h3>
                            <br>
                            <br>
                            <p>
                            <a href="${s.link}" class="manageSeriesBtn" role="button">View Episodes</a>
                            </p>
                        </div>
                    </c:forEach>
                </c:if>
            </div>
        </div>
    </div>
    <%@include file="footer.jsp"%>
</div>
</body>
</html>
