<form id="account_setup" method="post" action="${uploadUrl}" enctype="multipart/form-data">
    <br/>
    <br/>
    <div class="heading text-center">
        <img class="dividerline" src="/img/sep.png" alt="">
        <h4>Account Setup</h4>
        <img class="dividerline" src="img/sep.png" alt="">
    </div>
    <div style="margin-left:130px">
    <br><br>
    Pick your name! This is the name that will be displayed to other users. <br>
    Note: Only letters, numbers, and "-","_" allowed.
    <br><br>
        <label for="username">Desired username:</label>
        <span>
            <input name="username" id="username" size="36" type="text" value="" aria-required="true" oninput="isAlphanumeric();validateUser();">
                    <span id="name_error">
                        Error: name too short.
                        <br><span class = "stylespacing"></span>
                    </span>
                    <span id="in_use_error">
                        Error: name already in use.
                        <br><span class = "stylespacing"></span>
                    </span>
                    <span id="char_error">
                        Error: input can only contain letters, numbers, "-", and "_".
                        <br><span class = "stylespacing"></span>
                    </span>
                </span>
    <br>
    Upload a profile picture! (must be .jpg or .png) <br><br>
                <span>
                    <label for="myFile">Profile Picture:</label>
                    <input type="file" id="myFile" name="myFile"/>
                </span>

    <br>
    <br>
    <label>Email:</label> ${user}
    <br>
    <br>
    <br>
    <label></label><input type="submit" id="submit_button" disabled="true" class="generic_button">
    </div>
</form>