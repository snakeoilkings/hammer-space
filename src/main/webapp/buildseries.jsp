<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/29/2016
  Time: 12:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Create Series</title>
    <%@include file="/head.html" %>

</head>

<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

        <c:choose>
            <c:when test="${(user != null) && (account == null)}">
                <div class="container">
                    <%@include file="/accountSetup.jsp" %>
                </div>
            </c:when>
        <c:otherwise>

    <div class="container">
        <br>
        <div class="papers text-center">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4>Build a New Series</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>
            <br>
            <br>
            <br>

            <form action="<%=BlobstoreServiceFactory.getBlobstoreService().createUploadUrl("/build_series") %>" method="post" enctype="multipart/form-data">
                <input type="text" name="author" value="${sessionScope.account}" hidden="true"/>
                <label for="title">Series Title</label>
                <input type="text" class="form-control" id="title" placeholder="title" name = "title" required="true">
                <br>
                <label for="desc">Description</label>
                <input type="text" class="form-control" id="desc" placeholder="description...." name = "description" required="true">
                <br>
                <label for="category">Category</label>
                <input type="text" class="form-control" id="category" placeholder="...." name="category" required="true">
                <br>
                <label for="file">Thumbnail Image</label>
                <input type="file" class="form-control" id="file" name = "file" required="true">
                <br>
                <button type="submit" class="generic_button" value="Create Series">Submit</button>
                <br>
                <br>
                <br>
                <br>
            </form>
        </div>
    </div>
    </c:otherwise>
    </c:choose>
    <%@ include file="footer.jsp" %>
</div>
</body>
</html>
