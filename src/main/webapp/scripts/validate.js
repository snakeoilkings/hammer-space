/**
 * Created by kATHRYN on 4/5/2016.
 */

    function validateUser() {
        var name = document.forms['account_setup'].username.value;
        if (name.length < 4) {
            document.getElementById('name_error').style.display = 'inline';
            document.getElementById('submit_button').disabled = true;
        }
        else {
            document.getElementById('name_error').style.display = 'none';
            document.getElementById('submit_button').disabled = false;

            var proper_chars = isAlphanumeric();

            if (proper_chars == true) {
                validate(name).done(function (value) {
                    if (value == "failure") {
                        document.getElementById('in_use_error').style.display = 'inline';
                        document.getElementById('submit_button').disabled = true;
                    }
                    else {
                        document.getElementById('in_use_error').style.display = 'none';
                        document.getElementById('submit_button').disabled = false;
                        return true;
                    }
                });
            }
            else {
                document.getElementById('submit_button').disabled = true;
            }
        }
    }

    function validate(username) {
        return $.ajax({
            type: "POST",
            url: "/validate_account",
            data: {
                username: username
            },
            success: function()
            {
                console.log("reached servlet");
            },
            error: function ()
            {
                console.log("failure to contact servlet");
            }
        });
    }

function isAlphanumeric() {
    str = document.forms['account_setup'].username.value;
    var ok = /^[0-9a-zA-Z_-]+$/.test(str);

    if (ok == true)
        document.getElementById('char_error').style.display = 'none';
    else
        document.getElementById('char_error').style.display = 'inline';

    return ok;
}