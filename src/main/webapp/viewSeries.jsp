<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/21/2016
  Time: 9:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>${series.name}</title>
    <%@include file="/head.html" %>

    <script>
        function delete1(form_num) {
            var lookup = '#' + form_num;
            $(lookup).submit();
        }
    </script>

</head>

<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

    <c:choose>
    <c:when test="${(user != null) && (account == null)}">
    <div class="container">
        <%@include file="/accountSetup.jsp" %>
    </div>
    </c:when>
    <c:otherwise>

    <div class="container">
        <br>
        <div class="papers text-center">
            <br>
            <br>
            <img src="${series.pic}" style="max-width:300px;" id = "thumbnailImg">
            <h2 id = "titleName">${series.name}</h2>
            <h3 style = "color:white;">${series.description}</h3>
            <br>
            <br>
            <c:if test="${(not empty account) && (sessionScope.account==series.author)}">
                <span>
                    <form action="/manage_series" method="post" style="display:inline-block;">
                        <input type="hidden" name="seriesID" value="${seriesID}">
                        <input type="submit" class="generic_button" value="Edit Your Series Settings">
                    </form>
                    <form action="/buildepisode.jsp" method="post" style="display:inline-block;">
                        <input type="text" name = "seriesID" hidden="true" value="${seriesID}">
                        <input type="submit" value="Add a New Episode" class = "generic_button">
                    </form>
                </span>
            </c:if>


        <c:forEach var="t" items="${series.episodes}">
        <c:if test="${t.finished==true || t.finished==null}">
            <ul>
                <li>
                    <a href="/view/${t.comicID}">
                        <img class="media-object" src = "${t.urlString}" style = "width: 170px; height: 170px;">
                    </a>
                    <h4>${t.title}</h4><br>
                    <c:if test="${(not empty account) && (sessionScope.account==series.author)}">
                        <a href="/view/${t.comicID}" id = "viewBtn">
                        <h4>View</h4>
                        </a>

                        <form action="/manage_episode" method="post">
                            <input type="hidden" name="comicID" value="${t.comicID}">
                            <input type="submit" class="generic_button" value="Edit Your Episode Info">
                        </form>
                        <form method="post" action="/save_episode" id="del${t.comicID}">
                            <input type="text" name="delete" hidden="true" value="true">
                            <input type="text" name="comicID" hidden="true" value="${t.comicID}">
                            <a onclick="delete1('del${t.comicID}');" id = "deleteBtn">Delete</a>
                        </form>
                        <br>
                    </c:if>
                </li>
            </ul>
        </c:if>
        <c:if test="${t.finished==false && t.finished !=null && (not empty account) && (sessionScope.account==series.author)}">
            <ul>
                <li>
                <img src = "http://s32.postimg.org/jmiakmcc5/Work_in_Progress.jpg" style = "width: 170px; height: 170px;">
                    <h4>In Progress: ${t.title}</h4>
                    <br>
                    <form action="/draw" method="post">
                        <input type="hidden" name="comicID" value="${t.comicID}">
                        <input type="submit" class="generic_button" value="Finish Your Episode">
                    </form>
                </li>
            </ul>
        </c:if>
        </c:forEach>
    </div>
</div>
</c:otherwise>
</c:choose>

    <%@include file="footer.jsp"%>
</div>
</body>
</html>