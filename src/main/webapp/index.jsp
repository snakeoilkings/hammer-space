<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 3/31/2016
  Time: 11:13 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Hammerspace Comics</title>
    <%@ include file="/head.html"%>
</head>

<body>

<c:choose>
<c:when test="${(user != null) && (account == null)}">
    <div class="wrapper" id="wrapper">
        <%@include file="/header.jsp"%>
    <div class="container">
        <%@include file="/accountSetup.jsp" %>
    </div>
            </div>
</c:when>
<c:otherwise>

<div class="wrapper" id="wrapper">
    <header>
        <div class="menu">
            <div class="navbar-wrapper">
                <div class="container">
                    <div class="navwrapper">
                        <div class="navbar navbar-inverse navbar-static-top">
                            <div class="container">
                                <div class="navArea">
                                    <div class="navbar-collapse collapse" style="display:inline-block;">
                                        <div class="heading text-center" style="display:inline-block;">
                                            <h2><a href = "/">Hammerspace Comics</a></h2>
                                        </div>
                                        <ul class="nav navbar-nav" style="display:inline-block; float:right;">
                                                <c:choose>
                                                <c:when test="${user == null}">
                                                    <li class="menuItem"><a href="${loginUrl}">Login</a></li>
                                                    <li class="menuItem"><a href="${loginUrl}" >Sign Up</a></li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="menuItem"><a href="/profile/${sessionScope.account}">Welcome ${account}!</a></li>
                                                    <li class="menuItem"><a href="/manageAccount.jsp">My Account</a></li>
                                                    <li class="menuItem"><a href="${logoutUrl}">Log Out</a></li>
                                                </c:otherwise>
                                            </c:choose>
                                            <li class="menuItem"><a href="/help.jsp">Help</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner row" id="banner">
            <div class="parallax text-center" style="background-image: url(/img/screen.jpg);">
                <div class="parallax-pattern-overlay">
                    <div class="container text-center" style="height:430px;padding-top:100px;">
                        <img id="site-title" class=" wow fadeInDown" wow-data-delay="0.0s" wow-data-duration="0.9s" src="/img/logo.png" alt="logo"/>
                        <h2 class="intro wow zoomIn" wow-data-delay="0.4s" wow-data-duration="0.9s">Find a Comic</h2>
                        <form action="/search" method="get" style="display: inline-block">
                            <select id = "dropdown" name="search_type" style = "width: 120px; height: 33px;">
                                <option class = "dropMenu" value = "all" selected>ALL</option>
                                <option class = "dropMenu" value = "episode">Episode</option>
                                <option class = "dropMenu" value = "series">Series</option>
                                <option class = "dropMenu" value = "hashtag">Hashtag</option>
                                <option class = "dropMenu" value = "description">Description</option>
                                <option class = "dropMenu" value = "category">Category</option>
                                <option class = "dropMenu" value = "author">Author</option>
                            </select>
                            <input type="text" name="search_params">
                            <input type="submit" onclick="searchBarFunc()" value="Search"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--gallery-->
    <section class="gallery" id="gallery">
        <div class="container">
            <div class="heading text-center" style="display:inline-block;">
                <h3>Popular This Week</h3>
            </div>
            <div id="grid-gallery" class="grid-gallery">
                <section class="grid-wrap">
                    <ul class="grid">
                        <li class="grid-sizer"></li><!-- for Masonry column width -->
                        <li>
                            <a href = "${pop_link1}">
                            <figure>
                                <img src="${pop_pic1}" alt=""/>
                                <figcaption><h3>${pop_name1}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${pop_link2}">
                            <figure>
                                <img src="${pop_pic2}" alt=""/>
                                <figcaption><h3>${pop_name2}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${pop_link3}">
                            <figure>
                                <img src="${pop_pic3}" alt=""/>
                                <figcaption><h3>${pop_name3}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${pop_link4}">
                            <figure>
                                <img src="${pop_pic4}" alt=""/>
                                <figcaption><h3>${pop_name4}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
        <div class="container">
            <div class="heading text-center" style="display:inline-block;">
                <h3>Comedy</h3>
            </div>
            <div class="grid-gallery">
                <section class="grid-wrap">
                    <ul class="grid">
                        <li class="grid-sizer"></li>
                        <li>
                            <a href = "${link1}">
                            <figure>
                                <img src="${pic1}" alt=""/>
                                <figcaption><h3>${title1}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${link2}">
                            <figure>
                                <img src="${pic2}" alt=""/>
                                <figcaption><h3>${title2}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${link3}">
                            <figure>
                                <img src="${pic3}" alt=""/>
                                <figcaption><h3>${title3}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                        <li>
                            <a href = "${link4}">
                            <figure>
                                <img src="${pic4}" alt=""/>
                                <figcaption><h3>${title4}</h3></figcaption>
                            </figure>
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </section>
        </c:otherwise>
    </c:choose>
    <%@ include file="footer.jsp" %>
</body>
</html>