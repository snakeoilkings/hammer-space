<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/26/2016
  Time: 2:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Episode Settings</title>
    <%@include file="/head.html" %>

    <script>
        function delete1() {
            $("#del").submit();
        }
    </script>
</head>

<body>
<div class="wrapper" id="wrapper">
<%@include file="/header.jsp"%>

<c:choose>
    <c:when test="${(user != null) && (account == null)}">
        <div class="container">
            <%@include file="/accountSetup.jsp" %>
        </div>
    </c:when>
    <c:otherwise>
        <div class="container">
        <br>
            <div class="papers text-center">
                <div class="heading text-center">
                    <br/>
                    <br/>
                    <br/>
                    <img class="dividerline" src="/img/sep.png" alt="">
                    <h4>Episode Settings</h4>
                    <img class="dividerline" src="/img/sep.png" alt="">
                </div>

            <form action="/save_episode" method="post">
                <input type="text" name="comicID" hidden="true" value="${comicID}">
                <input type="text" name="delete" hidden="true" value="false">
                <br>
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" value="${title}" required="true">
                <br>
                <label for="desc">Description</label>
                <input type="text" class="form-control" id="desc" name="description" value="${description}" required="true">
                <br>
                <label for="hashtag">Hashtag</label>
                <input type="text" class="form-control" id="hashtag" name="hashtag" value="${hashtag}" required="true">
                <br>
                <br>
                <button type="submit" class="generic_button">Save</button>
            </form>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            </span>Want to delete this episode?

                <form method="post" action="/save_episode" id="del">
                    <input type="text" name="delete" hidden="true" value="true">
                    <input type="text" name="comicID" hidden="true" value="${comicID}"> <a onclick="delete1();">click here</a>.
                </form>
            </span>
        </div>
        </div>
    </c:otherwise>
</c:choose>

    <%@ include file="footer.jsp" %>

</div>
</body>
</html>
