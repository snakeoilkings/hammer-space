<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/29/2016
  Time: 12:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Create an Episode</title>
    <%@include file="/head.html" %>

    <script>
        function validate_episode() {
            var x = $("#upload").val();
            if (x == "") {
                alert("Please choose a file to upload, or select the option to build an episode on the site.");
                $("#upload").attr("required","true");
            }
        }

        function create_own() {
            $("#upload").removeAttr("required");
            $('#buildnew').val("true");
        }

    </script>
</head>

<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

    <c:choose>
        <c:when test="${(user != null) && (account == null)}">
        <div class="container">
            <%@include file="/accountSetup.jsp" %>
        </div>
        </c:when>
    <c:otherwise>

    <div class="container">
        <br>
        <div class="papers text-center">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4>Build a New Episode</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>

            <form id="bne" action="<%=BlobstoreServiceFactory.getBlobstoreService().createUploadUrl("/build_episode") %>" method="post" enctype="multipart/form-data">
                <input type="text" name="seriesID" value="<c:out value="${param.seriesID}" default = "${requestScope.seriesID}"/>" hidden="true"/>
                <input type="text" name="author" value="${sessionScope.account}" hidden="true"/>
                <input type="text" name="buildnew" id="buildnew" value="false" hidden="true"/>
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" placeholder="title" name="title" required="true">
                <br>
                <label for="desc">Description</label>
                <input type="text" class="form-control" id="desc" placeholder="description...." name="description" required="true">
                <br>
                <label for="hashtag">Hashtag</label>
                <input type="text" class="form-control" id="hashtag" placeholder="hashtags ex. #romance #adventure" name="hashtags" required="true">
                <br>
                <label for="upload">To upload an image file:</label>
                <input type="file" class="form-control" id="upload" name="file" >
                <br>
                <span>
                    <button type="submit" class="generic_button" onclick="validate_episode()">Upload Episode</button>
                    <button type="submit" class="generic_button" onclick="create_own()">Or Click Here to Draw Your Own!</button>
                </span>
                <br><br><br>
                <br><br><br>

            </form>
        </div>
        </div>

        </c:otherwise>
    </c:choose>
    <%@ include file="footer.jsp" %>
</div>

</body>
</html>