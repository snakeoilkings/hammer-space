<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/21/2016
  Time: 8:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<head>
    <title>Account Deletion</title>
    <%@include file="/head.html" %>
</head>
<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>

    <c:choose>
    <c:when test="${(user != null) && (account == null)}">
    <div class="container">
        <%@include file="/accountSetup.jsp" %>
    </div>
    </c:when>
    <c:otherwise>

    <div class="container">
        <br/>
        <div class="papers text-center">
            <div class="heading text-center">
                <br/>
                <br/>
                <br/>
                <img class="dividerline" src="/img/sep.png" alt="">
                <h4>Delete Account</h4>
                <img class="dividerline" src="/img/sep.png" alt="">
            </div>
        <br/>
        <br/>
        <br/>
        <br/>
            <div class="thumbnail">
                <h3> Are you sure you want to delete your account?</h3>
                <br>
                <h4>This will permanently remove all of your settings, series, and episodes. They cannot be restored.</h4>
                <br/>
                <br/>
                <form method="post" action="delete_account">
                    <input type="submit" value="Submit" class="generic_button"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" class="generic_button" onclick="window.location.href='/manageAccount.jsp'" value="Cancel"/>
                </form>
                <br>
                <br>
            </div>
        </div>
    </div>
        </c:otherwise>
        </c:choose>

        <%@ include file="footer.jsp" %>
</body>
</html>
