<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 4/21/16
  Time: 9:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Comic</title>
    <%@include file="/head.html" %>

    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(document).ready( function() {
        var cid = ${seriesID};
        $.ajax({
            type: "POST",
            url: "/subscribe",
            data: {
                seriesID: cid,
                action: "recent"
            }
        });
    });

    function subscribe() {
        document.getElementById("sub_button").onclick = function() {};
        $(document).ready(function () {
            var username = "${account}";
            var cid = ${seriesID};
            $.ajax({
                type: "POST",
                url: "/subscribe",
                data: {
                    username: username,
                    seriesID: cid,
                    action: "subscribe"
                }
            });
        });
    }

    function like() {
        document.getElementById("likeButton").onclick = function() {};
        $(document).ready(function () {
            var cid = ${comicID};
            $.ajax({
                type: "POST",
                url: "/subscribe",
                data: {
                    comicID: cid,
                    action: "like"
                },
                success: function () {
                    $('#total_likes').html(${likes}+1);
                }
            });
        });
    }
    </script>
</head>
<body>
<div class="wrapper" id="wrapper">
    <%@include file="/header.jsp"%>
    <br>
    <div class="container papers">
        <div class="text-center">
            <br>
            <br>
            <br>
            <h1>${series}</h1>
            <h2>${title}</h2>
            <h2>Written by <a href = "/profile/${author}">${author}</a></h2>
            <br>
            <br>
            <span>
                <h1 class = "prevButton" style="display:inline"><a href="${prev}">◄       </a></h1>
                <img class = "comicImage" src = "${comic}">
                <h1 class = "nextButton" style="display:inline"><a href="${next}">       ►</a></h1>
            </span>
        <div>
            <input type="image" id = "likeButton" src ="http://megaicons.net/static/img/icons_sizes/19/240/128/heart-icon.png" width="30px" height = "30px" onclick="like()"/>
            <span id="total_likes">${likes}</span> &nbsp;&nbsp;&nbsp;
            <c:if test="${(user != null) && (subbed == null)}">
                <input type ="button" id = "sub_button" value="subscribe to this series" class="btn btn-primary" onclick="subscribe();"/>
            </c:if>
            <div class="fb-share-button" data-href="${comic}" data-layout="button_count" data-mobile-iframe="true"></div>
        </div>

            <br>
            <br>
            <br>
            <br>
            <c:if test="${(user != null) && (account != null)}">
                <form action="/addComment" method="post">
                    <textarea rows="4" cols="50" style = "width: 80%; margin: auto;" name="commentForm">
                    </textarea>
                    <input type="text" value="${account}" hidden="true" name="accountForm">
                    <input type="text" value="${comicID}" hidden="true" name="comicIDForm">
                    <br>
                    <br>
                    <input type="submit" value="Submit Comment">
                </form>
                <br>
            </c:if>
        </div>
        <hr>
        <c:forEach var="c" items="${comments}">
            <span>
                <h6 style="color:#cc580c; font-weight: bold; display: inline-block;"><c:out value="${c.author}" /></h6>
                <h6 style="display: inline-block;"><c:out value="${c.time}" /></h6>
            <br>
            <c:out value="${c.comment}" />
            </span>
            <hr>
        </c:forEach>
        <br>
        <br>
    </div>
</div>
<%@ include file="footer.jsp" %>
</body>
</html>
