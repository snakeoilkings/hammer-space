<%--
  Created by IntelliJ IDEA.
  User: Kathryn
  Date: 5/3/16
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<!--footer-->
<section class="footer" id="footer">
    <p class="text-center">
        <a href="#wrapper" class="gototop"><i class="fa fa-angle-double-up fa-2x"></i></a>
    </p>
    <div class="container">
        <ul>
            <li><a href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
            <li><a href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
            <li><a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="http://www.pintinterest.com"><i class="fa fa-pinterest"></i></a></li>
            <li><a href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
        </ul>
        <p>
            &copy; 2015 Copyright Hammerspace Comics<br>
            Theme by <a href="http://www.wowthemes.net">WowThemes.Net</a>
        </p>
    </div>
</section>

</div><!--wrapper end-->

<!--Javascripts-->
<script src="/js/jquery.js"></script>
<script src="/js/modernizr.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/menustick.js"></script>
<script src="/js/parallax.js"></script>
<script src="/js/easing.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/js/masonry.js"></script>
<script src="/js/imgloaded.js"></script>
<script src="/js/classie.js"></script>
<script src="/js/colorfinder.js"></script>
<script src="/js/gridscroll.js"></script>
<script src="/js/contact.js"></script>
<script src="/js/common.js"></script>

<script type="text/javascript">
    jQuery(function($) {
        $(document).ready( function() {
            //enabling stickUp on the '.navbar-wrapper' class
            $('.navbar-wrapper').stickUp({
                parts: {
                    0: 'banner',
                    1: 'aboutus',
                    2: 'specialties',
                    3: 'gallery',
                    4: 'feedback',
                    5: 'contact'
                },
                itemClass: 'menuItem',
                itemHover: 'active',
                topMargin: 'auto'
            });
        });
    });
</script>